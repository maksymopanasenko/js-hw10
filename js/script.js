'use strict';

document.addEventListener('DOMContentLoaded', () => {
    const tabsParent = document.querySelector('.tabs');

    tabsParent.addEventListener('click', (e) => {
        const target = e.target;

        if (target.nodeName != 'LI') return;
        
        updateContent(target);
    });

    function updateContent(tab) {
        const tabs = document.querySelectorAll('.tabs-title');
        const tabItems = document.querySelectorAll('.tabs-item');

        tabItems.forEach((item, i) => item.dataset.content === tab.dataset.tab ? item.classList.add('show') : item.classList.remove('show'));
        tabs.forEach((item, i) => item.dataset.tab === tab.dataset.tab ? item.classList.add('active') : item.classList.remove('active'));
    }
});